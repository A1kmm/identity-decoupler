{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE UndecidableInstances #-}
module Crypto.PRandom where
import Crypto.Random
import Polysemy
import Data.ByteArray (ByteArray)

data PRandom m a where
  PRGetRandomBytes :: ByteArray byteArray => Int -> PRandom m byteArray

makeSem ''PRandom

runPRandom :: (MonadRandom m, Embed m `Member` r) => Sem (PRandom ': r) a -> Sem r a
runPRandom = interpret $ \case
  PRGetRandomBytes i -> embed $ getRandomBytes i

instance PRandom `Member` r => MonadRandom (Sem r) where
  getRandomBytes i = pRGetRandomBytes i
