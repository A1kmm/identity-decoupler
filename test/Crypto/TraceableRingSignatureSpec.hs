{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE OverloadedStrings #-}
module Crypto.TraceableRingSignatureSpec where

import Test.Hspec
import Test.Hspec.QuickCheck
import Test.QuickCheck
import Crypto.Random
import Crypto.TraceableRingSignature
import Crypto.ECC.Edwards25519
import Data.ByteArray (convert)
import qualified Data.ByteString as BS
import Crypto.PRandom
import Polysemy
import Polysemy.Error

instance MonadRandom Gen where
    getRandomBytes n = do
      rawData <- BS.pack <$> vectorOf n arbitrary
      return $ convert rawData

instance Arbitrary Point where
  arbitrary = toPoint <$> scalarGenerate
instance Arbitrary RingMember where
  arbitrary = RingMember <$> arbitrary
instance Arbitrary SignatureTag where
  arbitrary = SignatureTag <$> arbitrary <*> (BS.pack <$> arbitrary)
instance Arbitrary KeyPair where
  arbitrary = runFinal . embedToFinal @Gen . runPRandom @Gen $ genKeyPair

ringWithKeyPair :: Gen (KeyPair, SignatureTag)
ringWithKeyPair = do
  kp@(KeyPair _ pub) <- arbitrary
  ringMembersHead <- arbitrary
  ringMembersTail <- arbitrary
  tag <- SignatureTag
    (ringMembersHead ++ (RingMember pub):ringMembersTail) <$>
    (BS.pack <$> arbitrary)
  pure (kp, tag)

ringWithDifferentKeyPair :: Gen (KeyPair, SignatureTag, SignatureTag)
ringWithDifferentKeyPair = do
  kpGood@(KeyPair _ pubGood) <- arbitrary
  kpBad@(KeyPair _ pubBad) <- arbitrary
  ringMembersHead <- arbitrary
  ringMembersTail <- arbitrary
  msg <- BS.pack <$> arbitrary
  let tagBad = SignatureTag
        (ringMembersHead ++ (RingMember pubBad):ringMembersTail) msg
  let tagGood = SignatureTag
        (ringMembersHead ++ (RingMember pubGood):ringMembersTail) msg
  pure (kpBad, tagBad, tagGood)

spec :: Spec
spec = do
  describe "hashSignatureTagToPoint" $ do
    prop "Generates points on curve" $ \t -> pointHasPrimeOrder <$> hashSignatureTagToPoint t `shouldBe` (Right True)
  describe "signingProtocol" $ do
    prop "Generates signatures verificationProtocol accepts" $
      forAll ringWithKeyPair $ \(kp, tag) -> \msg ->
          let mt = MessageAndTag (BS.pack msg) tag
          in do
            ret <- runFinal @Gen . embedToFinal @Gen . runPRandom @Gen . runError @String $ do
              sig <- signingProtocol mt kp
              verificationProtocol mt sig
            return $ ret `shouldBe` (Right ())
    prop "Rejects signatures from non-member" $
      forAll ringWithDifferentKeyPair $ \(kpBad, tagBad, tagGood) -> \msg ->
          let
            mtBad = MessageAndTag (BS.pack msg) tagBad
            mtGood = MessageAndTag (BS.pack msg) tagGood
          in do
            ret <- runFinal @Gen . embedToFinal @Gen . runPRandom @Gen . runError @String $ do
              sig <- signingProtocol mtBad kpBad
              verificationProtocol mtGood sig
            return $ ret `shouldBe` (Left "Signature not valid")
    prop "Rejects signatures for different message" $
      forAll ringWithKeyPair $ \(kp, tag) -> \msg ->
          let mt1 = MessageAndTag (BS.pack msg) tag
              mt2 = MessageAndTag ("2" <> BS.pack msg) tag
          in do
            ret <- runFinal @Gen . embedToFinal @Gen . runPRandom @Gen . runError @String $ do
              sig <- signingProtocol mt1 kp
              verificationProtocol mt2 sig
            return $ ret `shouldBe` (Left "Signature not valid")
